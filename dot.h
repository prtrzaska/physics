#pragma once

#ifndef dot_h
#define dot_h

#include "ofMain.h"

class dot {
public:
	dot();
	~dot();

	void run();
	void draw();
	void move();
	/*void movingM(float x,float y);
	int map(int value, int minA, int maxA, float minB, float maxB);
*/
private:
	ofVec2f velocity; //These are a 2-dimensional vector
	ofVec2f location; 
	ofVec2f acceleration; //speed
	int diameter;
	int velX;
	int velY;
};


#endif /* dot_h */
