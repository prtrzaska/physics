#include "dot.h"



dot::dot()
{

	velX = -4+(ofGetMouseX() / 100); //tracing position of the x axis to alternate position of the cannon
	velY = -4;
	velocity.set(velX, velY);

	location.set(ofGetWidth()/2, ofGetHeight()/2);
	acceleration.set(0, 0.05);
	diameter = 40;

}



dot::~dot()
{
}

void dot::run()
{
	dot::draw();
	dot::move();
	//dot::movingM(ofGetMouseX(), ofGetMouseY());
	
}

void dot::draw()
{
	ofSetColor(0, 128, 255);
	ofFill();
	ofDrawEllipse(location.x, location.y, diameter, diameter);
}



void dot::move()
{
	velocity = velocity + acceleration;
	location = location + velocity;
}
//void dot::movingM(float x, float y)
//{
//	velocity = velocity*x/1000 + acceleration; //depend how far x mouse position is velocity changes by x position of mouse / 1000 + acc
//	
//	location = location + velocity;
//}
//int dot::map(int value,int minA,int maxA,float minB, float maxB) { //i dont think its working, math might be wrong
//	return (1 - ((value - minA) / (maxA - minA))) * minB + ((value - minA) / (maxA - minA)) * maxB;
//}

